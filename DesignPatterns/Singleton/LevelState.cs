﻿namespace DesignPatterns.Singleton
{
    public class LevelState
    {
        private int points = 0;
        public int Points
        {
            get => points; 
            set
            {
                points += value;
            } 
        }
        public int Lifes { get; set; }

        public LevelState()
        {
            Points = GameState.Instance().Points;
            Lifes = GameState.Instance().Lifes;
        }

        public void CompleteLevel()
        {
            SaveData();
        }

        public void LostLife()
        {
            Lifes--;
            if (Lifes == 0)
            {
                GameOver();
            }
        }

        public string ShowInformation()
        {
            return $"Points: {Points}, Lifes: {Lifes}";
        }

        private void GameOver()
        {
            Console.WriteLine("GAME OVER");
            SaveData();
            GameState.Instance().ShowGameState();
            Console.WriteLine("Saving points to DB");
            GameState.Instance().ResetGame();
            GameState.Instance().ShowGameState();
        }

        private void SaveData()
        {
            GameState.Instance().Points = this.Points;
            GameState.Instance().Lifes = this.Lifes;
        }
    }
}
