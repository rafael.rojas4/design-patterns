﻿namespace DesignPatterns.Singleton
{
    public class GameState
    {
        private static GameState? _instance = null;
        public static GameState Instance()
        {
            if (_instance == null)
            {
                _instance = new GameState();
            }

            return _instance;
        }

        public int Points { get; set; }

        public int Lifes { get; set; }

        private GameState() 
        {
            this.ResetGame();
        }

        public void ResetGame()
        {
            Points = 0;
            Lifes = 2;
        }

        public void ShowGameState()
        {
            Console.WriteLine($"Points: {Points}");
            Console.WriteLine($"Lifes: {Lifes}");
        }
    }
}
