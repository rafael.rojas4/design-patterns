﻿// See https://aka.ms/new-console-template for more information
using DesignPatterns.factoryMethod.factories;
using DesignPatterns.Singleton;
using FactoryMethod.Adapter;
using FactoryMethod.Adapter.factory;
using FactoryMethod.Proxy;
using DesignPatterns.Observer;
using FactoryMethod.ChainOfResponsibility;
using FactoryMethod.ChainOfResponsibility.factory;

#region Factory Method
/*Console.WriteLine("Get a weapon: 1:Heavy Machine gun, 2:Shotgun, 3:Rocket Launcher");
var userInput = Console.ReadLine();
var result = "";

switch (userInput)
{
    case "1":
        result = (new HeavyMachineGunFactory()).GetWeaponInformation();
        break;
    case "2":
        result = (new ShotgunFactory()).GetWeaponInformation();
        break;
    case "3":
        result = (new RocketLauncherFactory()).GetWeaponInformation();
        break;
    default:
        result = "no gun obtained";
        break;
}

Console.WriteLine(result);*/

#endregion

#region Singleton
/*var level1 = new LevelState();
Console.WriteLine("Init level 1: " + level1.ShowInformation());

level1.Points = 10;
level1.Points = 100;
level1.Points = 20;

Console.WriteLine(level1.ShowInformation() + "\n");
level1.CompleteLevel();

var level2 = new LevelState();
Console.WriteLine("Init level 2: " + level2.ShowInformation());

level2.Points = 70;
level2.Points = 1000;
level2.LostLife();
Console.WriteLine(level2.ShowInformation() + "\n");
level2.CompleteLevel();

var level3 = new LevelState();
Console.WriteLine("Init level 3: " + level3.ShowInformation());

level3.LostLife();*/

#endregion

#region Adapter

/*IMessager messager = new MessageHandler();

messager.sendMessage("Holi!...");

TelegramMessageHandler telegramMessageHandler = new TelegramMessageHandler();
IMessager messager02 = new TelegramMessageAdapter(telegramMessageHandler);

messager02.sendMessage("Bye! ..");*/

//FACTORY METHOD
/*IMessager messager03 = new MessageHandlerFactory().create();
messager03.sendMessage("Hi Adapter!");

IMessager messager04 = new TelegramMessageFactory().create();
messager04.sendMessage("Bye Adapter! ");*/

#endregion

#region Proxy
/*TextHandler textHandler = new TextHandler();
User user = new User(Rol.Manager, "username", "password");
IText proxy = new Proxy(textHandler, user);

proxy.write("Some text is being showed...");
proxy.write("loving design patterns");
Console.WriteLine(proxy.read());


User jrdev = new User(Rol.JrDev, "dev", "1234");
IText proxyJr = new Proxy(textHandler, jrdev);
proxyJr.write("   me too!");
Console.WriteLine(proxyJr.read());*/

// TextHandler textHandler = new TextHandler();
// User user = new User(Rol.Dev, "username", "password");
// IText proxy = new Proxy2(textHandler, user);
// proxy.write("Some text is being showed...");
// proxy.write("loving design patterns");
// Console.WriteLine(proxy.read());
#endregion

#region Observer
/*var sub1 = new GroupMember("John");
var sub2 = new GroupMember("Chavelo");
var sub3 = new GroupMember("Sergia");

MessagePublisher group = new MessagePublisher();

group.addGroupMember(sub1);
group.addGroupMember(sub2);
group.addGroupMember(sub3);

group.postMessage(new Message(sub1.Name, "Hello everyone!!!"));
group.removeGroupMember(sub1);

group.postMessage(new Message(sub2.Name, "La tuya por si acaso"));*/
#endregion

#region ChainOfResponsibility
// ICrimeHandler childDivision = new ChildDivision();
// ICrimeHandler ciberCrimeDivision = new CiberCrimeDivision();
// ICrimeHandler corruptionDivision = new CorruptionDivision();
// ICrimeHandler murderDivision = new MurderDivision();

// childDivision.setNextHandler(ciberCrimeDivision);
// ciberCrimeDivision.setNextHandler(corruptionDivision);
// corruptionDivision.setNextHandler(murderDivision);

ICrimeHandler policeDepartment = (new PoliceDepartmentFactory()).create();

Crime childCrime = new Crime(CrimeType.Child, CrimeSeverity.Minor);
Crime ciberCrime = new Crime(CrimeType.CiberCrime, CrimeSeverity.Major);
Crime corruptionCrime = new Crime(CrimeType.Corruption, CrimeSeverity.Minor);
Crime murderCrime = new Crime(CrimeType.Steal, CrimeSeverity.Critical);

var childSentence = policeDepartment.applySentence(childCrime);
Console.WriteLine("Punishment: " + childSentence.Punishment + "       days: " + childSentence.Days);

var ciberSentence = policeDepartment.applySentence(ciberCrime);
Console.WriteLine("Punishment: " + ciberSentence.Punishment + "        days: " + ciberSentence.Days);

var curruptionSentence = policeDepartment.applySentence(corruptionCrime);
Console.WriteLine("Punishment: " + curruptionSentence.Punishment + "        days: " + curruptionSentence.Days);

var murderSentence = policeDepartment.applySentence(murderCrime);
Console.WriteLine("Punishment: " + murderSentence.Punishment + "        days: " + murderSentence.Days);
#endregion


