﻿namespace DesignPatterns.Observer
{
    public interface IObserver<T>
    {
        public void execute(T message);
    }
}
