﻿namespace DesignPatterns.Observer
{
    public class GroupMember : IObserver<Message>
    {
        public string Name { get; set; }

        public GroupMember(string name)
        {
            this.Name = name;
        }
        public void execute(Message message)
        {
            Console.WriteLine($"message: {message.Text} from:{message.UserName} to: {Name}");
        }
    }
}
