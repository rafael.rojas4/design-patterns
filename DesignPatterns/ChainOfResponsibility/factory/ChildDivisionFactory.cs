using FactoryMethod.ChainOfResponsibility.senteceFactory;
namespace FactoryMethod.ChainOfResponsibility.factory
{
    public class ChildDivisionFactory : ICrimeHandlerCreator
    {
        public ICrimeHandler create()
        {
            ISentenceCreator senteceFactory = new SentenceFactory();
            return new ChildDivision(senteceFactory);
        }
    }
}