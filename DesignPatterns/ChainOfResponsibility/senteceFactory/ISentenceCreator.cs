using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.ChainOfResponsibility.senteceFactory
{
    public interface ISentenceCreator
    {
        public ISentence create();
    }
}