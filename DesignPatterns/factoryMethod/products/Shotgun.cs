﻿namespace DesignPatterns.factoryMethod.products
{
    public class Shotgun : IProduct
    {
        public string GetWeaponName()
        {
            return "Shotgun";
        }

        public string GetBulletType()
        {
            return "Remington Slugger 12/70";
        }

        public int GetNumberOfBullets()
        {
            return 25;
        }
    }
}
