﻿namespace DesignPatterns.factoryMethod.products
{
    public class RocketLauncher : IProduct
    {
        public string GetWeaponName()
        {
            return "RocketLauncher";
        }

        public string GetBulletType()
        {
            return "anti tank rocket";
        }

        public int GetNumberOfBullets()
        {
            return 50;
        }
    }
}
