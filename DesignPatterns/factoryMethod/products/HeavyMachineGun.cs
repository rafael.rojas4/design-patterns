﻿namespace DesignPatterns.factoryMethod.products
{
    public class HeavyMachineGun : IProduct
    {
        public string GetWeaponName()
        {
            return "HeavyMachineGun";
        }

        public string GetBulletType()
        {
            return ".50 cal";
        }

        public int GetNumberOfBullets()
        {
            return 200;
        }
    }
}
