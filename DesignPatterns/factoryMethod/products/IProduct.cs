﻿namespace DesignPatterns.factoryMethod.products
{
    public interface IProduct
    {
        public string GetWeaponName();
        public string GetBulletType();
        public int GetNumberOfBullets();
    }
}
