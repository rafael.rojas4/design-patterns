﻿using DesignPatterns.factoryMethod.products;

namespace DesignPatterns.factoryMethod.factories
{
    public class ShotgunFactory : Factory
    {
        public override IProduct CreateProduct()
        {
            return new Shotgun();
        }
    }
}
