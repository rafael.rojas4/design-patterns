﻿using DesignPatterns.factoryMethod.products;

namespace DesignPatterns.factoryMethod.factories
{
    public class HeavyMachineGunFactory : Factory
    {
        public override IProduct CreateProduct()
        {
            return new HeavyMachineGun();
        }
    }
}
