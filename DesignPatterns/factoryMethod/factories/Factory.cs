﻿using DesignPatterns.factoryMethod.products;

namespace DesignPatterns.factoryMethod.factories
{
    public abstract class Factory
    {
        public abstract IProduct CreateProduct();

        public string GetWeaponInformation()
        {
            var product = CreateProduct();
            return "Weapon: " + product.GetWeaponName() + " bullets: " + product.GetNumberOfBullets() + " " + product.GetBulletType();
        }
    }
}
