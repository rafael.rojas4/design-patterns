﻿using DesignPatterns.factoryMethod.products;

namespace DesignPatterns.factoryMethod.factories
{
    public class RocketLauncherFactory : Factory
    {
        public override IProduct CreateProduct()
        {
            return new RocketLauncher();
        }
    }
}
