namespace FactoryMethod.Proxy
{
    public class Proxy : IText
    {
        private TextHandler realTextHandler;
        private User user;
        public Proxy(TextHandler textHandler, User user){
            this.realTextHandler = textHandler;
            this.user = user;
        }
        private Boolean checkAccess(){
            if (this.user.Rol == Rol.Owner || this.user.Rol == Rol.Manager){
                return true;
            }else{
                return false;
            }
        }
        public string read()
        {
            return this.realTextHandler.read();
        }

        public void write(string text)
        {
            if (checkAccess()){
                this.realTextHandler.write(text);
            }else{
                Console.WriteLine("Access Denied! :(");
            }
        }
    }
}