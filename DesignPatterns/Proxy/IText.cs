namespace FactoryMethod.Proxy
{
    public interface IText
    {
        void write(String text);
        String read();
    }
}