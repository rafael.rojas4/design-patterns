using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Proxy
{
    public abstract class AbstractProxy: IText
    {
        protected TextHandler realTextHandler;
        protected User user;
        
        public AbstractProxy(TextHandler textHandler, User user){
            this.realTextHandler = textHandler;
            this.user = user;
        }
        public abstract Boolean checkAccess();
        public string read()
        {
            return this.realTextHandler.read();
        }

        public void write(string text)
        {
            if (checkAccess()){
                this.realTextHandler.write(text);
            }else{
                Console.WriteLine("Access Denied! :(");
            }
        }
    }
}