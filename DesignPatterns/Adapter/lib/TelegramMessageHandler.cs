
using FactoryMethod.Adapter.lib;

namespace FactoryMethod.Adapter
{
    public class TelegramMessageHandler
    {
        public TMessage createTMessage(String name, String message){
           return new TMessage(name, message); 
        }

        public TResponse send(TMessage tMessage){
            Console.WriteLine("Mensaje Adapter: " + tMessage.Message);
            return new TResponse(true, "");
        }
    }
}