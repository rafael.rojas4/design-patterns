using FactoryMethod.Adapter.lib;

namespace FactoryMethod.Adapter
{
    public class TelegramMessageAdapter : IMessager
    {
        private TelegramMessageHandler telegramMessageHandler;

        public TelegramMessageAdapter(TelegramMessageHandler telegramMessageHandler){
            this.telegramMessageHandler = telegramMessageHandler;
        }
        public bool sendMessage(string message)
        {
            var tMessage = this.telegramMessageHandler.createTMessage(getUserName(), message);
            var response = this.telegramMessageHandler.send(tMessage);
            return getMessageHandlerResponse(response);
        }

        private String getUserName(){
            return "Gilmar";
        }

        private Boolean getMessageHandlerResponse(TResponse tResponse){
            return tResponse.Success;
        }
    }
}