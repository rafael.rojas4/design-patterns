using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Adapter.factory
{
    public abstract class MessagerCreator
    {
        abstract public IMessager create();
    }
}