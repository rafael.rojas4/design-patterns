
namespace FactoryMethod.Adapter.factory
{
    public class TelegramMessageFactory : MessagerCreator
    {
        public override IMessager create()
        {
            TelegramMessageHandler telegramMessageHandler = new TelegramMessageHandler();
            return new TelegramMessageAdapter(telegramMessageHandler);
        }
    }
}