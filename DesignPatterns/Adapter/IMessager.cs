using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Adapter
{
    public interface IMessager
    {
        Boolean sendMessage(string messaage);
    }
}